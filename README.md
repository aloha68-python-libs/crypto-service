Projet permettant de chiffrer et de déchiffrer des chaînes de caractères

## Commandes

Les commandes sont interactives et plutôt simples, il suffit de les exécuter via *poetry run*. 

### encrypt-secret

On saisit un mot de passe et un grain de sel (password + salt) pour ensuite pouvoir chiffrer des messages saisis.

### encrypt-keyring

Les mots de passe keyring sont stockés grâce à un nom de service et un utilisateur. Cette commande commence donc par demander ces deux informations. Si un mot de passe est trouvé, on peut directement chiffrer un ou plusieurs messages de manière interactive. Si le mot de passe n'existe pas, on peut le créer et il s'enregistre automatiquement dans le keyring.

### delete-keyring

Commande basique permettant de supprimer un mot de passe dans le keyring pour un nom de service et un utilisateur donné.

## Utilisation en tant que bibliothèque

### Chiffrement basique

Pour chiffrer ou déchiffrement simplement à partir d'un mot de passe et d'un grain de sel, on peut utiliser la classe **CryptoService**.

    from aloha68_crypto_service import CryptoService, get_random_salt

    salt = get_random_salt()
    cs = CryptoService("un-super-password", salt)
    
    message = "Je m'appelle Jean-Louis"
    message_chiffre = cs.encrypt(message)
    message_dechiffre = cs.decrypt(message_chiffre)

### Chiffrement depuis un keyring

Il est possible d'utiliser un keyring en tant que gestionnaire de mot de passe pour chiffrer et déchiffrer ses messages. Pour cela, nous utilisons la classe **KeyringCryptoService** qui possède les mêmes méthodes *encrypt* et *decrypt* que son parent **CryptoService**.

    cs = KeyringCryptoService("nom-de-service", "nom-utilisateur")

    # Si aucun mot de passe n'est trouvé dans le keyring
    if not cs.is_usable:
        cs.save_password_and_salt("un-super-password", "du-sel-à-gogo")

Attention, l'utilisation d'un keyring est basique et il n'est pas possible de changer le backend de cette base. Il faudrait creuser plus en détail pour cela !

