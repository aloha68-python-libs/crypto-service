from aloha68_crypto_service import CryptoService, get_random_salt

import pytest
import unittest


def test_get_random_salt():
    """ Teste la longueur correct du grain de sel aléatoire """
    length = 5
    salt = get_random_salt(length)
    assert length == len(salt)


class TestCryptoService(unittest.TestCase):
    """ Tests sur la classe CryptoService """

    message: str = "Thomas le boss !"

    def test_encryption(self):
        """ Teste le chiffrement d'un message """

        cs = CryptoService(password="un-super-mot-de-passe", salt="un-grain-de-sel")
        enc = cs.encrypt(self.message)
        dec = cs.decrypt(enc)
        self.assertEqual(self.message, dec)

    def test_unattainable_password_and_salt(self):
        """ Teste que le mot de passe et le grain de sel ne sont pas atteignables """

        cs = CryptoService(password="un-super-mot-de-passe", salt="un-grain-de-sel")
        self.assertEqual(cs.password, "")
        self.assertEqual(cs.salt, "")

    def test_wrong_password(self):
        """ Teste le déchiffrement avec un mauvais mot de passe """

        cs = CryptoService(password="un-super-mot-de-passe", salt="un-grain-de-sel")
        enc = cs.encrypt(self.message)
        cs.password = "un-mauvais-mot-de-passe"

        with pytest.raises(ValueError):
            cs.decrypt(enc)

    def test_wrong_salt(self):
        """ Teste le déchiffrement avec un mauvais grain de sel """

        cs = CryptoService(password="un-super-mot-de-passe", salt="un-grain-de-sel")
        enc = cs.encrypt(self.message)
        cs.salt = "un-mauvais-grain-de-sel"

        with pytest.raises(ValueError):
            cs.decrypt(enc)
