from aloha68_crypto_service import KeyringCryptoService, CryptoService

import keyring
import unittest


class TestKeyringCryptoService(unittest.TestCase):
    """ Teste sur la classe KeyringCryptoService """

    message: str = "Thomas le boss !"
    tested_keyring_service_name: str = "test"
    tested_keyring_username: str = "test"
    tested_keyring_password: str = "test"
    tested_keyring_salt: str = "1234"

    def test_encrypt_from_keyring(self):
        """ Teste le chiffrement d'un message à partir de keyring """

        # On enregistre un keyring à utiliser
        data = self.tested_keyring_password + KeyringCryptoService.SEPARATOR + self.tested_keyring_salt
        keyring.set_password(self.tested_keyring_service_name, self.tested_keyring_username, data)

        kcs = KeyringCryptoService(self.tested_keyring_service_name, self.tested_keyring_username)
        enc = kcs.encrypt(self.message)
        dec = kcs.decrypt(enc)
        self.assertEqual(self.message, dec)

        cs = CryptoService(self.tested_keyring_password, self.tested_keyring_salt)
        cs_enc = cs.encrypt(self.message)
        cs_dec = cs.decrypt(cs_enc)
        self.assertEqual(dec, cs_dec)

        # On supprime le keyring
        keyring.delete_password(self.tested_keyring_service_name, self.tested_keyring_username)

    def test_create_keyring(self):
        """ Teste l'enregistrement d'un nouveau mot de passe en keyring """

        kcs = KeyringCryptoService(self.tested_keyring_service_name, self.tested_keyring_username)
        kcs.save_password_and_salt(self.tested_keyring_password, self.tested_keyring_salt)

        # On teste la récupération depuis la méthode de KeyringCryptoService
        password, salt = kcs.get_password_and_salt_from_keyring()
        self.assertEqual(password, self.tested_keyring_password)
        self.assertEqual(salt, self.tested_keyring_salt)

        # On teste la donnée réelle depuis keyring
        data = self.tested_keyring_password + kcs.SEPARATOR + self.tested_keyring_salt
        keyring_data = keyring.get_password(self.tested_keyring_service_name, self.tested_keyring_username)
        self.assertEqual(data, keyring_data)

        # On supprime le jeton crée
        keyring.delete_password(self.tested_keyring_service_name, self.tested_keyring_username)

    def test_delete_keyring(self):
        """ Teste la suppression d'un mot de passe de keyring """

        # On crée une donnée dans le keyring
        data = self.tested_keyring_password + KeyringCryptoService.SEPARATOR + self.tested_keyring_salt
        keyring.set_password(self.tested_keyring_service_name, self.tested_keyring_username, data)

        # Suppression de la donnée
        kcs = KeyringCryptoService(self.tested_keyring_service_name, self.tested_keyring_username)
        kcs.delete_from_keyring()

        data = keyring.get_password(self.tested_keyring_service_name, self.tested_keyring_username)
        self.assertIsNone(data)
