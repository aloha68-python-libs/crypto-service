# Changelog

Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/) et ce projet adhère à la [gestion sémantique de version](https://semver.org/lang/fr/).

## [0.3.4] - 2022-01-17

### Changement
- Modification du nom de méthode dans KeyringCryptoService: save_password_and_salt

## 0.3.3 - 2022-01-17

Cette version a été sautée sans faire attention...

## [0.3.2] - 2022-01-17

### Nouveau
- Boucle de chiffrement de messages pour les deux commandes de chiffrement

## [0.3.1] - 2022-01-15

### Changement
- Configuration de pylint et coverage
- Modifications mineures dans *crypto_service.py* pour rendre pylint heureux
- Modifications du gitlab-CI pour ajouter un paquet de keyring par défaut (pour les tests unitaires)

## [0.3.0] - 2022-01-15

### Nouveau
- Classe KeyringCryptoService pour chiffrer et déchiffrer depuis le keyring
- Tests associés à la classe KeyringCryptoService
- Commandes *encrypt-keyring* et *delete-keyring* pour manipuler les entrées keyring

## [0.2.0] - 2022-01-14

### Changement
- Réécriture du code en utilisant une classe CryptoService

## [0.1.0] - 2021-10-28

### Nouveau
- Deux méthodes permettant de chiffrer et déchiffrer des messages à partir d'un mot de passe
- Prise en charge de python 3.7 et supérieur


[0.3.4]: https://gitlab.com/aloha68-python-libs/crypto-service/-/tags/0.3.4
[0.3.2]: https://gitlab.com/aloha68-python-libs/crypto-service/-/tags/0.3.2
[0.3.1]: https://gitlab.com/aloha68-python-libs/crypto-service/-/tags/0.3.1
[0.3.0]: https://gitlab.com/aloha68-python-libs/crypto-service/-/tags/0.3.0
[0.2.0]: https://gitlab.com/aloha68-python-libs/crypto-service/-/tags/0.2.0
[0.1.0]: https://gitlab.com/aloha68-python-libs/crypto-service/-/tags/0.1.0

