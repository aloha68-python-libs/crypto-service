"""
Module permettant de chiffrer et déchiffrer des messages à l'aide de mot de passe
"""

import base64
import logging
import random
import string
from typing import Tuple

import keyring

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet, InvalidToken


logger = logging.getLogger(__name__)


def get_random_salt(length: int = 20) -> str:
    """ Génère un grain de sel aléatoire """
    return "".join(random.choice(string.ascii_letters) for _ in range(length))


class CryptoService:
    """ Classe permettant le chiffrement ou le déchiffrement de messages """

    def __init__(self, password: str, salt: str):
        self.__password: bytes = password.encode()
        self.__salt: bytes = salt.encode()
        self.fernet: Fernet = self.generate_fernet()

    @property
    def password(self) -> str:
        """ Mot de passe (définition seulement, impossible à lire) """
        return ""

    @password.setter
    def password(self, value: str):
        bytes_value = value.encode()
        if bytes_value != self.__password:
            self.__password = bytes_value
            self.fernet = self.generate_fernet()

    @property
    def salt(self) -> str:
        """ Grain de sel (définition seulement, impossible à lire) """
        return ""

    @salt.setter
    def salt(self, value: str):
        bytes_value = value.encode()
        if bytes_value != self.__salt:
            self.__salt = bytes_value
            self.fernet = self.generate_fernet()

    def generate_fernet(self) -> Fernet:
        """ Génère une clé à partir du mot de passe et du grain de sel """

        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=self.__salt,
            iterations=100000,
            backend=default_backend()
        )
        key = base64.urlsafe_b64encode(kdf.derive(self.__password))
        return Fernet(key)

    def encrypt(self, message: str) -> str:
        """ Chiffre un message """
        encrypted_message = self.fernet.encrypt(message.encode())
        return encrypted_message.decode()

    def decrypt(self, encrypted_message: str) -> str:
        """ Déchiffre un message """

        try:
            message = self.fernet.decrypt(encrypted_message.encode())
        except InvalidToken as ex:
            raise ValueError("Mauvais password !") from ex

        return message.decode()


class KeyringCryptoService(CryptoService):
    """
    Classe permettant le chiffrement et déchiffrement de messages
    à partir de mot de passe stocké dans un keyring
    """

    # Jeton qui sépare le mot de passe du grain de sel
    SEPARATOR = "::!::"

    def __init__(self, service_name: str, username: str):
        self.service_name = service_name
        self.username = username
        self.is_usable: bool = False

        password, salt = self.get_password_and_salt_from_keyring()
        super().__init__(password, salt)

    def get_password_and_salt_from_keyring(self) -> Tuple[str, str]:
        """ Récupère le mot de passe et le grain de sel depuis keyring """

        data = keyring.get_password(self.service_name, self.username)
        logger.debug(f"KeyringCryptoService: "
                     f"On récupère {data} dans {self.service_name}:{self.username}")

        if data and self.SEPARATOR in data:
            password, salt = data.split(self.SEPARATOR, maxsplit=2)
            self.is_usable = True
            return password, salt

        return "", ""

    def save_password_and_salt(self, password: str, salt: str):
        """ Enregistre le mot de passe et le grain de sel dans keyring """

        data = password + self.SEPARATOR + salt

        logger.debug(f"KeyringCryptoService: "
                     f"On enregistre {data} dans {self.service_name}:{self.username}")
        keyring.set_password(self.service_name, self.username, data)

        self.password = password
        self.salt = salt

    def delete_from_keyring(self):
        """ Supprime le mot de passe dans le keyring """
        logger.debug(f"KeyringCryptoService: "
                     f"On supprime la donnée de {self.service_name}:{self.username}")
        keyring.delete_password(self.service_name, self.username)
