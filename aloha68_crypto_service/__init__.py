"""
Module permettant de chiffrer et déchiffrer des messages à l'aide de mot de passe
"""
import getpass
import keyring
import keyring.errors

from .crypto_service import CryptoService, KeyringCryptoService, get_random_salt


__all__ = ["get_random_salt", "CryptoService", "KeyringCryptoService"]


def encryption_loop(crypto: CryptoService):
    """ Boucle permettant la saisie et le chiffrement d'un ou plusieurs messages
    :param crypto: Service de cryptographie à utiliser
    :return:
    """
    while True:
        message = input("\nMessage (vide pour quitter) : ")
        if not message:
            break

        encrypted_message = crypto.encrypt(message)
        print(f"Message chiffré : {encrypted_message}")

        decrypted_message = crypto.decrypt(encrypted_message)
        print(f"Message déchiffré : {decrypted_message}")


def encrypt_secret():
    """ Chiffre un message avec un mot de passe et un grain de sel """

    password = getpass.getpass("Mot de passe : ")
    salt = input("Grain de sel (optionnel) : ") or get_random_salt()

    crypto = CryptoService(password, salt)
    encryption_loop(crypto)


def encrypt_from_keyring():
    """ Chiffre un message depuis un mot de passe enregistré dans un keyring """

    service = input("Nom du service keyring : ")
    username = input("Utilisateur du service keyring : ")

    crypto = KeyringCryptoService(service, username)

    # Si aucun mot de passe n'est renseigné
    if not crypto.is_usable:

        print("\nAucun mot de passe stocké pour ce service et cet utilisateur !")
        password = getpass.getpass("Mot de passe : ")
        salt = input("Grain de sel (optionnel) : ")

        # Si pas de grain de sel
        if not salt:
            salt = get_random_salt()
            print(f"Grain de sel généré : {salt}")

        # On enregistre les données dans le keyring
        crypto.save_password_and_salt(password, salt)

    # Saisie et chiffrement de message
    encryption_loop(crypto)


def delete_from_keyring():
    """ Supprime un mot de passe stocké dans keyring """

    while True:
        service = input("Service (vide pour quitter) : ")
        if not service:
            break

        username = input("Utilisateur : ")

        try:
            keyring.delete_password(service, username)
            print("OK")
        except keyring.errors.PasswordDeleteError:
            print("Aucun mot de passe")

        print("")
